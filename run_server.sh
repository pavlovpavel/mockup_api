#!/usr/bin/env bash

source api_venv/bin/activate

echo "exec: FLASK_APP=app/api.py"
export FLASK_APP=app/api.py

echo "exec: FLASK_DEBUG=1"
export FLASK_DEBUG=1

echo "Starting..."
sleep 2

flask run -h 0.0.0.0 -p 5555 &
