from flask import Flask
from flask_restful import Resource, Api, reqparse

from core.Query import *
from core.Coords import *
from core.Generator import *

app = Flask(__name__)
api = Api(app)
parser = reqparse.RequestParser()
parser2 = reqparse.RequestParser()

# json pretty print
settings = app.config.get('RESTFUL_JSON', {})
settings.setdefault('indent', 4)
settings.setdefault('sort_keys', True)
app.config['RESTFUL_JSON'] = settings


class GetMockups(Resource):
    def post(self):

        # number of mockups per page
        parser.add_argument('per_page', required=True)
        # page number
        parser.add_argument('page', required=True)

        args = parser.parse_args()

        # parse form-data arguments
        per_page = args['per_page']
        page = args['page']

        q = QuerySet(int(per_page), int(page))

        return q.query(), {'Access-Control-Allow-Origin': '*'}


class ParseCoords(Resource):
    def post(self):

        parser2.add_argument('id')
        parser2.add_argument('coords')

        args2 = parser2.parse_args()

        mockup_id = args2['id']
        coords = args2['coords']

        c = Coords(int(mockup_id), coords)
        msg = c.write_coords()

        return msg, {'Access-Control-Allow-Origin': '*'}


class GenerateFinals(Resource):
    def get(self):
        result = generate_finals()

        return result, {'Access-Control-Allow-Origin': '*'}

api.add_resource(GenerateFinals, '/generate', strict_slashes=False)
api.add_resource(GetMockups, '/get_mockups', strict_slashes=False)
api.add_resource(ParseCoords, '/post_coords', strict_slashes=False)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5555, debug=False)
