import sqlite3
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Coords(object):
    def __init__(self, mockup_id, coords):
        """
        :param mockup_id: int
        :param coords: str
        """
        self.mockup_id = mockup_id
        self.coords = coords

    def write_coords(self):
        """
        Write received coordinates into the database.

        :return: bool
        """
        try:
            conn = sqlite3.connect('/home/mockupgenerator/web/db/mockups.db')
            c = conn.cursor()
            # c.execute("INSERT INTO MOCKUPS (txt_coords) VALUES ('{0}') WHERE id={1}".format(self.coords, self.id))
            c.execute("UPDATE MOCKUPS SET txt_coords='{0}' WHERE id={1}".format(self.coords, self.mockup_id))
            conn.commit()
            conn.close()

            return dict(status=200,
                        message='Success')
        except Exception as err:
            return dict(status=400,
                        message=err)
