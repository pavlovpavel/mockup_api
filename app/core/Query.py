import collections
import sqlite3
import os
import math

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
GEN_DIR = '/home/mockupgenerator/web/'


class QuerySet(object):
    def __init__(self, per_page, page):
        """
        Create object that contains parameters for creating queryset.

        :param per_page: int
        :param page: int
        """

        self.per_page = per_page
        # prevent 0 page
        if page != 0:
            self.page = page
        else:
            self.page = 1

    def query(self):
        """
        Method which creates queryset based on class attributes

        :return: dict (application/json)
        """

        # TODO: get rif of hardcodding!
        HOST = '104.237.147.234:7733'

        conn = sqlite3.connect('/home/mockupgenerator/web/db/mockups.db')
        c = conn.cursor()
        # pagination for SELECT
        c.execute("SELECT id, mid, catid, gid, status, txt_coords FROM MOCKUPS "
                  "ORDER BY id "
                  "LIMIT {0} OFFSET ({0} * ({1} - 1))".format(self.per_page, self.page))
        result = c.fetchall()
        # total items in db
        c.execute("SELECT id FROM MOCKUPS WHERE status=1")
        total = len(c.fetchall())
        # empty dict for response
        response = dict()
        # general info about response
        info = dict(page='{0} of {1}'.format(self.page, int(math.ceil(float(total)/self.per_page))),
                    per_page=self.per_page,
                    total_items=total)
        response['info'] = info

        # index value
        count = 0
        # write images' info in response dict
        for row in result:
            id, mid, catid, gid, status, txt_coords = row

            green_path = os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-1.png.jpg').format(gid, catid, mid)
            final = os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-final.png').format(gid, catid, mid)

            final_ok = os.path.isfile(final)
            green_ok = os.path.isfile(green_path)

            if final_ok:
                mockup_path = 'http://{0}/mockups/{1}_Mockups/{2}/Final/{3}/{3}-final.png'.format(HOST,
                                                                                                  gid,
                                                                                                  catid,
                                                                                                  mid)

            else:
                mockup_path = 'http://{0}/mockups/{1}_Mockups/{2}/Final/{3}/{3}-1.png.jpg'.format(HOST,
                                                                                                  gid,
                                                                                                  catid,
                                                                                                  mid)

            mockup_name = '{0}_{1}_{2}'.format(gid, catid, mid)
            ffile = dict(id=id,
                         name=mockup_name,
                         path=mockup_path,
                         coords=txt_coords,
                         status=status)
            response[count] = ffile
            count += 1

        return collections.OrderedDict(sorted(response.items()))
