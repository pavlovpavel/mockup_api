import sqlite3
import os
import subprocess
from PIL import Image

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
GEN_DIR = '/home/mockupgenerator/web/'
MOCKUP_DIR = '/home/mockupsgenerator/web/mockups/'


def generate_finals():
    # delete all final images
    subprocess.check_call(["find", MOCKUP_DIR, "-type", "f", "-name", "*.-final.png", "-delete"])

    # generate all final images
    conn = sqlite3.connect('/home/mockupgenerator/web/db/mockups.db')
    c = conn.cursor()
    # pagination for SELECT
    c.execute("SELECT id, mid, catid, gid FROM MOCKUPS ORDER BY id")
    result = c.fetchall()

    # write images' info in response dict
    try:
        for row in result:
            id, mid, catid, gid = row
            base_path = os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-0.png').format(gid, catid, mid)
            green_path = os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-1.png.jpg').format(gid, catid, mid)
            overlay_path = os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-2.png').format(gid, catid, mid)

            final = os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-final.png').format(gid, catid, mid)

            final_ok = os.path.isfile(final)
            base_ok = os.path.isfile(base_path)
            green_ok = os.path.isfile(green_path)
            overlay_ok = os.path.isfile(overlay_path)

            if final_ok:
                continue

            elif base_ok and overlay_ok:
                base = Image.open(base_path).convert('RGBA')
                bg = Image.new('RGBA', base.size, 'green')
                overlay = Image.open(overlay_path)
                bg.paste(base, None, base)
                bg.paste(overlay, None, overlay)
                bg.convert('RGB')
                bg.save(
                    os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-final.png').format(gid, catid, mid))

            elif green_ok and overlay_ok:
                green = Image.open(green_path).convert('RGBA')
                overlay = Image.open(overlay_path)
                green.paste(overlay, None, overlay)
                green.convert('RGB')
                green.save(
                    os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-final.png').format(gid, catid, mid))

            else:
                if base_ok:
                    base = Image.open(base_path).convert('RGBA')
                    bg = Image.new('RGBA', base.size, 'green')
                    bg.paste(base, None, base)
                    bg.convert('RGB')
                    bg.save(
                        os.path.join(GEN_DIR, 'mockups/{0}_Mockups/{1}/Final/{2}/{2}-final.png').format(gid, catid, mid))
    except:
        error = "Oops, something went wrong. Check layer filenames in 'mockups/{0}_Mockups/{1}/Final/{2}/'".format(gid, catid, mid)
        hint = dict(base='{mid}-0.png', red_green='{mid}-1.png.jpg', overlay='{mid}-2.png')
        return dict(error=error, layers=hint), 400

    return dict(msg="Successfully generated all images!"), 200
