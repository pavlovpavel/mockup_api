# README #
### Mockup_api ###

* Api for image map generator
* 0.1

### Setup ###

* Clone the project - $ git clone xxx
* Create virtualenv - $ ./create_venv.sh
* Start server - $ ./run_server.sh
* Stop server - $ ./stop_server.sh

### 'get_mockups' route ###

* Url: http://<host>:<port>/get_mockups
* Method: POST
---
* Param1: page - number of current page
* Param2: per_page - number of items per page
---
* Return: application/json

### 'post_coords' route ###

* Url: http://<host>:<port>/post_coord
* Method: POST
---
* Param1: id - id of edited mockup
* Param2: coords - it's coordinates
---
* Return: application/json