#!/usr/bin/env bash

# install venv
/usr/bin/pip install virtualenv
# create venv
virtualenv api_venv
# setup venv
api_venv/bin/pip install -r requirements.txt
